import { commonsTypeHasPropertyString, commonsTypeHasPropertyStringOrUndefined } from 'tscommons-es-core';

export type TDomain = {
		domain: string;
		ip?: string;
};

export function isTDomain(test: unknown): test is TDomain {
	if (!commonsTypeHasPropertyString(test, 'domain')) return false;
	if (!commonsTypeHasPropertyStringOrUndefined(test, 'ip')) return false;
	
	return true;
}
