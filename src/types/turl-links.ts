import { commonsTypeIsStringArrayEnumObject } from 'tscommons-es-core';

import { EDirection, isEDirection } from '../enums/edirection';

export type TUrlLinks = {
		[direction in EDirection]: string[]
};

export function isTUrlLinks(test: unknown): test is TUrlLinks {
	if (!commonsTypeIsStringArrayEnumObject<EDirection>(
			test,
			isEDirection
	)) return false;
	
	if (!Object.keys(test).includes(EDirection.INBOUND)) return false;
	if (!Object.keys(test).includes(EDirection.OUTBOUND)) return false;

	return true;
}
