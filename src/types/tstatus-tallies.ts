import { commonsTypeIsNumberEnumObject } from 'tscommons-es-core';

import { EStatus, isEStatus } from '../enums/estatus';

export type TStatusTallies = {
		[status in EStatus]?: number;
};

export function isTStatusTallies(test: unknown): test is TStatusTallies {
	return commonsTypeIsNumberEnumObject<EStatus>(test, isEStatus);
}
