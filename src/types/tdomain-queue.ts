import { commonsTypeHasPropertyNumber, commonsTypeHasPropertyString } from 'tscommons-es-core';

export type TDomainQueue = {
		domain: string;
		queue: number;
};

export function isTDomainQueue(test: unknown): test is TDomainQueue {
	if (!commonsTypeHasPropertyString(test, 'domain')) return false;
	if (!commonsTypeHasPropertyNumber(test, 'queue')) return false;
	
	return true;
}
