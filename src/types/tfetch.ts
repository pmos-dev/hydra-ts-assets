import { commonsTypeHasPropertyNumber, commonsTypeHasPropertyString } from 'tscommons-es-core';

export type TFetch = {
		index: number;
		url: string;
};

export function isTFetch(test: any): test is TFetch {
	if (!commonsTypeHasPropertyNumber(test, 'index')) return false;
	if (!commonsTypeHasPropertyString(test, 'url')) return false;

	return true;
}
