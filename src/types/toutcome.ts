import { commonsTypeHasPropertyBoolean, commonsTypeHasPropertyNumber, commonsTypeHasPropertyNumberOrUndefined } from 'tscommons-es-core';

export type TOutcome = {
		index: number;
		failed: boolean;
		statusCode?: number;
};

export function isTOutcome(test: any): test is TOutcome {
	if (!commonsTypeHasPropertyNumber(test, 'index')) return false;
	if (!commonsTypeHasPropertyBoolean(test, 'failed')) return false;
	if (!commonsTypeHasPropertyNumberOrUndefined(test, 'statusCode')) return false;

	return true;
}
