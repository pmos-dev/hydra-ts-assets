import { commonsTypeHasPropertyString } from 'tscommons-es-core';

export type TAspError = {
		application: string;
		message: string;
		stack: string;
};

export function isTAspError(test: unknown): test is TAspError {
	if (!commonsTypeHasPropertyString(test, 'application')) return false;
	if (!commonsTypeHasPropertyString(test, 'message')) return false;
	if (!commonsTypeHasPropertyString(test, 'stack')) return false;
	
	return true;
}
