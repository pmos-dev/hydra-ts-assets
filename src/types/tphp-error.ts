import { commonsTypeHasPropertyEnum, commonsTypeHasPropertyNumber, commonsTypeHasPropertyString } from 'tscommons-es-core';

import { EPhpErrorType, isEPhpErrorType } from '../enums/ephp-error-type';

export type TPhpError = {
		type: EPhpErrorType;
		message: string;
		file: string;
		line: number;
};

export function isTPhpError(test: unknown): test is TPhpError {
	if (!commonsTypeHasPropertyEnum<EPhpErrorType>(test, 'type', isEPhpErrorType)) return false;
	if (!commonsTypeHasPropertyString(test, 'message')) return false;
	if (!commonsTypeHasPropertyString(test, 'file')) return false;
	if (!commonsTypeHasPropertyNumber(test, 'line')) return false;
	
	return true;
}
