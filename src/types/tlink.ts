import { commonsTypeHasPropertyString } from 'tscommons-es-core';

export type TLink = {
		url: string;
		outgoing: string;
};

export function isTLink(test: unknown): test is TLink {
	if (!commonsTypeHasPropertyString(test, 'url')) return false;
	if (!commonsTypeHasPropertyString(test, 'outgoing')) return false;
	
	return true;
}
