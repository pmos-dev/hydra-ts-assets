import { commonsTypeIsString } from 'tscommons-es-core';

export enum EStatus {
		QUEUED = 'queued',
		DISALLOWED = 'disallowed',
		DENY = 'deny',
		ACTIVE = 'active',
		DONE = 'done',
		DEAD = 'dead',
		FAILED = 'failed',
		ARCHIVED = 'archived'
}

export function toEStatus(type: string): EStatus|undefined {
	switch (type) {
		case EStatus.QUEUED.toString():
			return EStatus.QUEUED;
		case EStatus.DISALLOWED.toString():
			return EStatus.DISALLOWED;
		case EStatus.DENY.toString():
			return EStatus.DENY;
		case EStatus.ACTIVE.toString():
			return EStatus.ACTIVE;
		case EStatus.DONE.toString():
			return EStatus.DONE;
		case EStatus.DEAD.toString():
			return EStatus.DEAD;
		case EStatus.FAILED.toString():
			return EStatus.FAILED;
		case EStatus.ARCHIVED.toString():
			return EStatus.ARCHIVED;
	}
	return undefined;
}

export function fromEStatus(type: EStatus): string {
	switch (type) {
		case EStatus.QUEUED:
			return EStatus.QUEUED.toString();
		case EStatus.DISALLOWED:
			return EStatus.DISALLOWED.toString();
		case EStatus.DENY:
			return EStatus.DENY.toString();
		case EStatus.ACTIVE:
			return EStatus.ACTIVE.toString();
		case EStatus.DONE:
			return EStatus.DONE.toString();
		case EStatus.DEAD:
			return EStatus.DEAD.toString();
		case EStatus.FAILED:
			return EStatus.FAILED.toString();
		case EStatus.ARCHIVED:
			return EStatus.ARCHIVED.toString();
	}
	
	throw new Error('Unknown EStatus');
}

export function isEStatus(test: unknown): test is EStatus {
	if (!commonsTypeIsString(test)) return false;
	
	return toEStatus(test) !== undefined;
}

export function keyToEStatus(key: string): EStatus {
	switch (key) {
		case 'QUEUED':
			return EStatus.QUEUED;
		case 'DISALLOWED':
			return EStatus.DISALLOWED;
		case 'DENY':
			return EStatus.DENY;
		case 'ACTIVE':
			return EStatus.ACTIVE;
		case 'DONE':
			return EStatus.DONE;
		case 'DEAD':
			return EStatus.DEAD;
		case 'FAILED':
			return EStatus.FAILED;
		case 'ARCHIVED':
			return EStatus.ARCHIVED;
	}
	
	throw new Error(`Unable to obtain EStatus for key: ${key}`);
}

export const ESTATUSS: EStatus[] = Object.keys(EStatus)
		.map((e: string): EStatus => keyToEStatus(e));
