import { commonsTypeIsString } from 'tscommons-es-core';

export enum EDirection {
		INBOUND = 'inbound',
		OUTBOUND = 'outbound'
}

export function toEDirection(type: string): EDirection|undefined {
	switch (type) {
		case EDirection.INBOUND.toString():
			return EDirection.INBOUND;
		case EDirection.OUTBOUND.toString():
			return EDirection.OUTBOUND;
	}
	return undefined;
}

export function fromEDirection(type: EDirection): string {
	switch (type) {
		case EDirection.INBOUND:
			return EDirection.INBOUND.toString();
		case EDirection.OUTBOUND:
			return EDirection.OUTBOUND.toString();
	}
	
	throw new Error('Unknown EDirection');
}

export function isEDirection(test: unknown): test is EDirection {
	if (!commonsTypeIsString(test)) return false;
	
	return toEDirection(test) !== undefined;
}

export function keyToEDirection(key: string): EDirection {
	switch (key) {
		case 'INBOUND':
			return EDirection.INBOUND;
		case 'OUTBOUND':
			return EDirection.OUTBOUND;
	}
	
	throw new Error(`Unable to obtain EDirection for key: ${key}`);
}

export const EDIRECTIONS: EDirection[] = Object.keys(EDirection)
		.map((e: string): EDirection => keyToEDirection(e));
