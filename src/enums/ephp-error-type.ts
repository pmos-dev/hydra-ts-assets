import { commonsTypeIsString } from 'tscommons-es-core';

export enum EPhpErrorType {
		CATCHABLEFATAL = 'catchablefatal',
		STRICT = 'strict',
		NOTICE = 'notice',
		WARNING = 'warning',
		FATAL = 'fatal',
		PARSE = 'parse'
}

export function toEPhpErrorType(type: string): EPhpErrorType|undefined {
	switch (type) {
		case EPhpErrorType.CATCHABLEFATAL.toString():
			return EPhpErrorType.CATCHABLEFATAL;
		case EPhpErrorType.STRICT.toString():
			return EPhpErrorType.STRICT;
		case EPhpErrorType.NOTICE.toString():
			return EPhpErrorType.NOTICE;
		case EPhpErrorType.WARNING.toString():
			return EPhpErrorType.WARNING;
		case EPhpErrorType.FATAL.toString():
			return EPhpErrorType.FATAL;
		case EPhpErrorType.PARSE.toString():
			return EPhpErrorType.PARSE;
	}
	return undefined;
}

export function fromEPhpErrorType(type: EPhpErrorType): string {
	switch (type) {
		case EPhpErrorType.CATCHABLEFATAL:
			return EPhpErrorType.CATCHABLEFATAL.toString();
		case EPhpErrorType.STRICT:
			return EPhpErrorType.STRICT.toString();
		case EPhpErrorType.NOTICE:
			return EPhpErrorType.NOTICE.toString();
		case EPhpErrorType.WARNING:
			return EPhpErrorType.WARNING.toString();
		case EPhpErrorType.FATAL:
			return EPhpErrorType.FATAL.toString();
		case EPhpErrorType.PARSE:
			return EPhpErrorType.PARSE.toString();
	}
	
	throw new Error('Unknown EPhpErrorType');
}

export function isEPhpErrorType(test: unknown): test is EPhpErrorType {
	if (!commonsTypeIsString(test)) return false;
	
	return toEPhpErrorType(test) !== undefined;
}

export function keyToEPhpErrorType(key: string): EPhpErrorType {
	switch (key) {
		case 'CATCHABLEFATAL':
			return EPhpErrorType.CATCHABLEFATAL;
		case 'STRICT':
			return EPhpErrorType.STRICT;
		case 'NOTICE':
			return EPhpErrorType.NOTICE;
		case 'WARNING':
			return EPhpErrorType.WARNING;
		case 'FATAL':
			return EPhpErrorType.FATAL;
		case 'PARSE':
			return EPhpErrorType.PARSE;
	}
	
	throw new Error(`Unable to obtain EPhpErrorType for key: ${key}`);
}

export const EPHP_ERROR_TYPES: EPhpErrorType[] = Object.keys(EPhpErrorType)
		.map((e: string): EPhpErrorType => keyToEPhpErrorType(e));

export function deriveEPhpErrorType(value: string): EPhpErrorType|undefined {
	if (value === 'Catchable fatal error') return EPhpErrorType.CATCHABLEFATAL;
	if (value === 'Strict Standards') return EPhpErrorType.STRICT;
	if (value === 'Notice') return EPhpErrorType.NOTICE;
	if (value === 'Warning') return EPhpErrorType.WARNING;
	if (value === 'Fatal error') return EPhpErrorType.FATAL;
	if (value === 'Parse error') return EPhpErrorType.PARSE;
	
	return undefined;
}
