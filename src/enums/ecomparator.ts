import { commonsTypeIsString } from 'tscommons-es-core';

export enum EComparator {
		GT = 'gt',
		LT = 'lt',
		GTE = 'gte',
		LTE = 'lte'
}

export function toEComparator(type: string): EComparator|undefined {
	switch (type) {
		case EComparator.GT.toString():
			return EComparator.GT;
		case EComparator.LT.toString():
			return EComparator.LT;
		case EComparator.GTE.toString():
			return EComparator.GTE;
		case EComparator.LTE.toString():
			return EComparator.LTE;
	}
	return undefined;
}

export function fromEComparator(type: EComparator): string {
	switch (type) {
		case EComparator.GT:
			return EComparator.GT.toString();
		case EComparator.LT:
			return EComparator.LT.toString();
		case EComparator.GTE:
			return EComparator.GTE.toString();
		case EComparator.LTE:
			return EComparator.LTE.toString();
	}
	
	throw new Error('Unknown EComparator');
}

export function isEComparator(test: unknown): test is EComparator {
	if (!commonsTypeIsString(test)) return false;
	
	return toEComparator(test) !== undefined;
}

export function keyToEComparator(key: string): EComparator {
	switch (key) {
		case 'GT':
			return EComparator.GT;
		case 'LT':
			return EComparator.LT;
		case 'GTE':
			return EComparator.GTE;
		case 'LTE':
			return EComparator.LTE;
	}
	
	throw new Error(`Unable to obtain EComparator for key: ${key}`);
}

export const ECOMPARATORS: EComparator[] = Object.keys(EComparator)
		.map((e: string): EComparator => keyToEComparator(e));
