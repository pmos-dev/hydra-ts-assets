import { commonsTypeHasPropertyEnum, commonsTypeHasPropertyString } from 'tscommons-es-core';

import { EStatus, isEStatus } from '../enums/estatus';

export type IUrl = {
		url: string;
		domain: string;
		status: EStatus;
};

export function isIUrl(test: unknown): test is IUrl {
	if (!commonsTypeHasPropertyString(test, 'url')) return false;
	if (!commonsTypeHasPropertyString(test, 'domain')) return false;
	if (!commonsTypeHasPropertyEnum<EStatus>(test, 'status', isEStatus)) return false;
	
	return true;
}
