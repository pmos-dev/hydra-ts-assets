import { commonsTypeHasPropertyArray, commonsTypeHasPropertyString } from 'tscommons-es-core';

export interface IUrlErrors<T> {
		url: string;
		errors: (T|true)[];
}

export function isIUrlErrors<T>(
		test: unknown,
		isT: (t: unknown) => t is T
): test is IUrlErrors<T> {
	if (!commonsTypeHasPropertyString(test, 'url')) return false;
	
	if (!commonsTypeHasPropertyArray(test, 'errors')) return false;
	
	for (const error of test.errors) {
		if (error === true) continue;
		if (!isT(error)) return false;
	}
	
	return true;
}
