import { commonsTypeHasPropertyNumber, commonsTypeHasPropertyString } from 'tscommons-es-core';

export interface IImage {
		url: string;
		size: number;
}

export function isIImage(test: unknown): test is IImage {
	if (!commonsTypeHasPropertyString(test, 'url')) return false;
	if (!commonsTypeHasPropertyNumber(test, 'size')) return false;

	return true;
}
